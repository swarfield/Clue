package gui;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import clueGame.*;

/**
 * Creates the main intractable panel for the game.
 * Clue's main logic resides in the action listeners of the class
 * @author Samuel Warfield
 * @author Ross Starritt
 */
public class ControlPlanelGUI extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField turnText;
	private JTextField rollText;
	private JTextField guessText;
	private JTextField responceText;
	private boolean playerNullTag;


	boolean isFirstTurn;
	int curTurn; // Keeps track of whose turn it is
	Board board;

	/**
	 * Default Constructors
	 */

	public ControlPlanelGUI() {
		// Create a layout with 2 rows
		setLayout(new GridLayout(2,0));
		isFirstTurn = true;
		playerNullTag = true;
		curTurn = 0;
		board = Board.getInstance();

		turnText = new JTextField();
		rollText = new JTextField();
		guessText = new JTextField();
		responceText = new JTextField();

		// Creates text field on whose turn, and the two intractable							//System.out.println(nextRoll);
		// buttons on the top row
		JPanel turnPanel = createTurnPanel();						
		add(turnPanel);

		// Displays Game State information
		JPanel guessPanel = createGuessPanel();
		add(guessPanel);

		setPreferredSize(new Dimension(900,100));
	}

	private JPanel createTurnPanel() {
		//panel to be modified
		JPanel panel = new JPanel();

		// Use a grid layout, 1 row, 4 elements (label, text, button, button)
		//panel.setLayout(new GridLayout(1,4));
		//Label
		JLabel turnLabel = new JLabel("Who's Turn?");
		//Text under label.
		turnText = new JTextField("                     ");
		turnText.setEditable(false); //this text is not user editable

		//add label to the panel
		panel.add(turnLabel);
		//add text field
		panel.add(turnText);

		//make buttons
		JButton nextPlayer = new JButton("Next Player");
		JButton accusation = new JButton("Make an accusation");

		nextPlayer.addActionListener(new TurnListener());
		accusation.addActionListener(new AccusationListener());

		panel.add(nextPlayer);
		panel.add(accusation);
		//panel.setBorder(new TitledBorder (new EtchedBorder(), ""));
		return panel;
	}

	//Handles the next turn aspect of ClueGame
	private class TurnListener implements ActionListener
	{
		//Handles the next turn aspect of ClueGame
		public void actionPerformed(ActionEvent e) 
		{
			// Get all players
			ArrayList<Player> allPlayers = board.getPlayers();


			if (board.getHumanTargeted().size() == 0) {
				if (isFirstTurn) { // initial game setup
					Player theHuman = board.getHuman();

					for (Player aPerson : allPlayers){
						if (aPerson == theHuman) {
							int nextRoll = board.getDieRoll();

							rollText.setText(Integer.toString(nextRoll));
							aPerson.pickLocation(nextRoll);

							curTurn += 1;
							break;
						}
						curTurn += 1;
					}
					isFirstTurn = false;
				} else {
					if (board.getHuman().getSuggestion() != null) {
						trySuggestion(board.getHuman());
						board.getHuman().resetSug();
					}
					// Default game state
					Player curPlayer = allPlayers.get(curTurn % allPlayers.size());

					int nextRoll = board.getDieRoll();
					//System.out.println(nextRoll);
					rollText.setText(Integer.toString(nextRoll));

					// Move the Player and try to create a suggestion
					if (playerNullTag) {
						curPlayer.pickLocation(nextRoll);				
					}
					System.out.println(curPlayer.getBoardCell().getInitial() );

					// If a player is in a room create a suggestion

					if(trySuggestion(curPlayer)) {
						playerNullTag = true;
					} else {
						playerNullTag = false;
					}

					//Update the turn O'Meter
					turnText.setText(curPlayer.getName());

					board.repaint();

					if (playerNullTag) {
						curTurn += 1;
					}
				}
			} else {
				new SplashScreen("Please complete your turn!");
			}
		}
	}

	/**
	 * This checks if the player suggestions are good
	 * @param curPlayer The current player
	 * @return if false the Human did not make a suggestion
	 */
	private boolean trySuggestion(Player curPlayer) {
		if (curPlayer.getBoardCell().getInitial() != 'W') {
			Solution aSuggestion = curPlayer.getSuggestion();

			if (aSuggestion == null) { // If the player has not created a suggestion
				return false;
			}

			// If the player hasn't entered anything do nothing
			ArrayList<Card> feedBack = board.handleSuggestion(curPlayer, aSuggestion);
			String responce = "";
			ArrayList<Card> theSuggestion = aSuggestion.getAnswer();
			String guess = "";
			
			
			
			for (Card aCard : feedBack) {
				responce += aCard.getName() + ' ';
			}

			for (Card aCard : theSuggestion) {
				guess += aCard.getName() + ' ';
			}

			responceText.setText(responce);
			guessText.setText(guess);

			return true;
		} else {
			return true;
		}

	}

	private class AccusationListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			// Check if it's the player's turn
			if (board.getHumanTargeted().size() != 0) {
				new GuessGui();
			}
			else { // ITS NOT YOU TURN YOU CHEATER
				new SplashScreen("It must be your turn!");
			}
			// If so check the Solution {true:Win, False:Lose})
		}
	}	

	private JPanel createGuessPanel() {
		JPanel panel = new JPanel();   // Final panel to be returned
		JPanel dieRoll = new JPanel(); //
		JPanel guess = new JPanel();   //
		JPanel result = new JPanel();  //

		//panel.setLayout(new GridLayout(1,0)); //die, guess, guess result

		//Setup Random Die
		JLabel rollLabel = new JLabel("Roll");
		rollText.setText(Integer.toString(board.getDieRoll()));
		rollText.setEditable(false);
		TitledBorder die = new TitledBorder(new EtchedBorder(), "Die");
		dieRoll.add(rollLabel);
		dieRoll.add(rollText);
		dieRoll.setBorder(die);
		panel.add(dieRoll);

		JLabel guessLabel = new JLabel("Guess");
		Solution theSoln = Board.getInstance().getSolution();

		String aGuess = new String();
		for (Card aCard : theSoln.getAnswer()) {
			aGuess = aGuess.concat(aCard.getName());
			aGuess = aGuess.concat(",");
		} //System.out.println(aGuess);


		guessText = new JTextField("                                                                              ");// I don't know of a better way to do this, so please don't change unless you are making it better
		//guessText.setSize(50, 1);
		guessText.setEditable(false);
		guess.add(guessLabel);
		guess.add(guessText);
		guess.setBorder(new TitledBorder (new EtchedBorder(), "Guess"));
		panel.add(guess);

		JLabel responce = new JLabel("Responce");
		responceText = new JTextField("                                                                                      ");// see above comment on the same shit
		responceText.setEditable(false);
		result.add(responce);
		result.add(responceText);
		result.setBorder(new TitledBorder (new EtchedBorder(), "Guess Result"));
		panel.add(result);

		return panel;
	}


}
