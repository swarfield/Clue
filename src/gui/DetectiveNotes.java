package gui;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import clueGame.*;

public class DetectiveNotes extends JDialog{

	private static final long serialVersionUID = 1L;

	public DetectiveNotes() {
		setSize(new Dimension(700,700));
		setTitle("Detective Notes");
		add(notesPanel());
	}
	
	private JPanel notesPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3,2));
		panel.add(peopleBoxes());
		panel.add(peopleOptions());
		panel.add(roomBoxes());
		panel.add(roomOptions());
		panel.add(weaponBoxes());
		panel.add(weaponOptions());
		return panel;
	}
	
	private JPanel peopleBoxes() {
		JPanel panel = new JPanel();
		ArrayList<Player> people = Board.getInstance().getPlayers();
		for (Player p: people) {
			JCheckBox boxyMcBoxFace = new JCheckBox(p.getName());
			panel.add(boxyMcBoxFace);
		}
		panel.setBorder(new TitledBorder (new EtchedBorder(), "People"));
		return panel;
	}
	
	private JPanel peopleOptions() {
		JPanel panel = new JPanel();
		ArrayList<Player> people = Board.getInstance().getPlayers();
		JComboBox<String> boxiBoi = new JComboBox<String>();
		for (Player p: people) {
			boxiBoi.addItem(p.getName());
		}
		panel.add(boxiBoi);
		panel.setBorder(new TitledBorder (new EtchedBorder(), "Person Guess"));
		return panel;
	}
	
	private JPanel roomBoxes() {
		JPanel panel = new JPanel();
		ArrayList<String> rooms = Board.getInstance().getRooms();
		for (String r: rooms) {
			JCheckBox boxyMcBoxFace = new JCheckBox(r);
			panel.add(boxyMcBoxFace);
		}
		panel.setBorder(new TitledBorder (new EtchedBorder(), "Rooms"));
		return panel;
	}
	
	private JPanel roomOptions() {
		JPanel panel = new JPanel();
		ArrayList<String> rooms = Board.getInstance().getRooms();
		JComboBox<String> boxiBoi = new JComboBox<String>();
		for (String r: rooms) {
			boxiBoi.addItem(r);
		}
		panel.add(boxiBoi);
		panel.setBorder(new TitledBorder (new EtchedBorder(), "Room Guess"));
		return panel;
	}
	
	private JPanel weaponBoxes() {
		JPanel panel = new JPanel();
		ArrayList<String> weapons = Board.getInstance().getWeapons();
		for (String p: weapons) {
			JCheckBox boxyMcBoxFace = new JCheckBox(p);
			panel.add(boxyMcBoxFace);
		}
		panel.setBorder(new TitledBorder (new EtchedBorder(), "Weapons"));
		return panel;
	}
	
	private JPanel weaponOptions() {
		JPanel panel = new JPanel();
		ArrayList<String> weapons = Board.getInstance().getWeapons();
		JComboBox<String> boxiBoi = new JComboBox<String>();
		for (String p: weapons) {
			boxiBoi.addItem(p);
		}
		panel.add(boxiBoi);
		panel.setBorder(new TitledBorder (new EtchedBorder(), "Weapon Guess"));
		return panel;
	}
}
