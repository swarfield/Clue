package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import clueGame.*;

public class ClueGameMainGui extends JFrame{

	private static final long serialVersionUID = 1L;

	public ClueGameMainGui() {
		setSize(new Dimension(1200,700));
		setTitle("Clue Game");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Create Menu bar 
		JMenuBar menu = new JMenuBar();
		menu.add(createFileMenu());
		setJMenuBar(menu);
		
		Board board = Board.getInstance();

		add(new ControlPlanelGUI(), BorderLayout.SOUTH);
		add(new HandGUI(), BorderLayout.EAST);
		add(board, BorderLayout.CENTER);
	}
	
	private JMenu createFileMenu() {
		JMenu menu = new JMenu("File");
		menu.add(createFileDetectiveItem());
		menu.add(createFileExitItem());
		return menu;
	}
	
	private JMenuItem createFileDetectiveItem() {
		JMenuItem item = new JMenuItem("Detective Notes");
		class MenuItemListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				DetectiveNotes dialog = new DetectiveNotes();
				dialog.setVisible(true);
			}	
		}
		item.addActionListener(new MenuItemListener());
		return item;
	}
	
	private JMenuItem createFileExitItem() {
		JMenuItem item = new JMenuItem("Exit");
		class MenuItemListener implements ActionListener{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);	
			}	
		}
		item.addActionListener(new MenuItemListener());
		return item;
	}
	
	public static void main(String[] args) {
		ClueGameMainGui gui = new ClueGameMainGui();
		gui.setVisible(true);
		
		new SplashScreen();
	}

}
