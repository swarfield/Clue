package defunctTests;

/**
 * This file tests that the config files are loaded properly.
 * @author Ross Starritt
 * @author Sam Warfield
 * Last Modified: 2018/03/05
 */

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import clueGame.BadConfigFormatException;
import clueGame.Board;
import clueGame.BoardCell;
import clueGame.DoorDirection;

public class ConfigLoadTests {

	// Constants that I will use to test whether the file was loaded correctly
	public static final int LEGEND_SIZE = 12;
	public static final int NUM_ROWS = 30;
	public static final int NUM_COLUMNS = 26;

	// NOTE: I made Board static because I only want to set it up one 
	// time (using @BeforeClass), no need to do setup before each test.
	private static Board board;

	@BeforeClass
	public static void setUp() {
		// Board is singleton, get the only instance
		board = Board.getInstance();
		// set the file names to use my config files
		board.setConfigFiles("Clues.csv", "roomKey.txt");		
		// Initialize will load BOTH config files 
		board.initialize();
	}

	@Test
	public void testRooms() throws FileNotFoundException { //tests the load of the legend!
		try {
			board.loadRoomConfig();
		} catch (BadConfigFormatException e) {
			System.out.println(e.getMessage());
			//something else should be done, but good enough for now
		}
		// Get the map of initial => room 
		Map<Character, String> legend = board.getLegend();
		// Ensure we read the correct number of rooms
		assertEquals(LEGEND_SIZE, legend.size());
		// To ensure data is correctly loaded, test retrieving a few rooms 
		// from the hash, including the first and last in the file and a few others
		assertEquals("Office", legend.get('O'));
		assertEquals("Study", legend.get('S'));
		assertEquals("Dormroom", legend.get('D'));
		assertEquals("Dining Room", legend.get('E'));
		assertEquals("Walkway", legend.get('W'));
		assertEquals("Garage", legend.get('G'));
	}
	
	@Test
	public void testBoardDimensions() {
		// Ensure we have the proper number of rows and columns (lifted from Baldwin's tests because it so elegantly tests)
		assertEquals(NUM_ROWS, board.getNumRows());
		assertEquals(NUM_COLUMNS, board.getNumColumns());		
	}
	
	@Test
	public void topLeft() {
		BoardCell cell = board.getCellAt(0,0);
		
		HashSet<BoardCell> testList = board.getAdjList(cell);
		assertTrue(testList.contains(board.getCellAt(1,0)));
		assertTrue(testList.contains(board.getCellAt(0,1)));
		assertEquals(2, testList.size());
	}
	
	@Test
	public void bottomRight() {
		BoardCell cell = board.getCellAt(NUM_ROWS -1, NUM_COLUMNS -1);
		HashSet<BoardCell> testList = board.getAdjList(cell);
		assertTrue(testList.contains(board.getCellAt(NUM_ROWS -2, NUM_COLUMNS -1)));
		assertTrue(testList.contains(board.getCellAt(NUM_ROWS -1, NUM_COLUMNS -2)));
		assertEquals(2, testList.size());
	}
	
	@Test
	public void leftEdge() {
		BoardCell cell = board.getCellAt(1,0);
		HashSet<BoardCell> testList = board.getAdjList(cell);
		assertTrue(testList.contains(board.getCellAt(0,0)));
		assertTrue(testList.contains(board.getCellAt(1,1)));
		assertTrue(testList.contains(board.getCellAt(2,0)));
		assertEquals(3, testList.size());
	}
	
	@Test
	public void col3MidGrid() {
		BoardCell cell = board.getCellAt(1,2);
		HashSet<BoardCell> testList = board.getAdjList(cell);
		assertTrue(testList.contains(board.getCellAt(0,2)));
		assertTrue(testList.contains(board.getCellAt(1,1)));
		assertTrue(testList.contains(board.getCellAt(2,2)));
		assertTrue(testList.contains(board.getCellAt(1,3)));
		assertEquals(4, testList.size());
	}
	
	@Test
	public void testTargets0_3()
	{
		BoardCell cell = board.getCellAt(0, 0);
		board.calcTargets(cell, 3);
		HashSet<BoardCell> targets = board.getTargets();
		assertEquals(6, targets.size());
		assertTrue(targets.contains(board.getCellAt(0, 3)));
		assertTrue(targets.contains(board.getCellAt(1, 2)));
		assertTrue(targets.contains(board.getCellAt(1, 0)));
		assertTrue(targets.contains(board.getCellAt(2, 1)));
		assertTrue(targets.contains(board.getCellAt(3, 0)));
		assertTrue(targets.contains(board.getCellAt(0, 1)));
	}

	@Test
	public void numDoors() {
		int rows = board.getNumRows();
		int cols = board.getNumColumns();
		int numDoors = 0;
		BoardCell cell;
		// Go threw all cells can count the doors
		for (int x = 0; x < cols; x++) {
			for (int y = 0; y < rows; y++) {
				cell = board.getCellAt(y, x);
				if (cell.isDoorway()) {
					numDoors++;
				}
			}
		}
		
		assertTrue(numDoors == 17);
	}
	
	// test each door direction and none
	@Test
	public void testDoorUp() {// this cell should have door direction up
		BoardCell cell = board.getCellAt(11,0);
		assertEquals(DoorDirection.UP, cell.getDoorDirection());
		assertTrue(cell.isDoorway());
	}
	
	@Test
	public void testDoorDown() {// this cell has a door down
		BoardCell cell = board.getCellAt(19, 5);
		assertEquals(DoorDirection.DOWN, cell.getDoorDirection());
		assertTrue(cell.isDoorway());
	}
	
	@Test
	public void testDoorLeft() {
		BoardCell cell = board.getCellAt(4, 19);
		assertEquals(DoorDirection.LEFT, cell.getDoorDirection());
		assertTrue(cell.isDoorway());
	}
	
	@Test
	public void testDoorRight() {
		BoardCell cell = board.getCellAt(0, 17);
		assertEquals(DoorDirection.RIGHT, cell.getDoorDirection());
		assertTrue(cell.isDoorway());
	}
	
	@Test
	public void testDoorNone() {
		BoardCell cell = board.getCellAt(12, 12);
		assertEquals(DoorDirection.NONE, cell.getDoorDirection());
		assertFalse(cell.isDoorway());
	}
	// correct number of doors
	// test cells for initial
	@Test
	public void testCellInitialK() {
		//29,21
		BoardCell cell = board.getCellAt(29, 25);
		assertEquals('K', cell.getInitial());
	}
	
	//10,20, C
	@Test
	public void testCellInitialC() {
		BoardCell cell = board.getCellAt(20, 10);
		assertEquals('C', cell.getInitial());
	}
	
	//4,28,G
	@Test
	public void testCellInitialG() {
		BoardCell cell = board.getCellAt(28, 4);
		assertEquals('G', cell.getInitial());
	}
	
	//0,0,B
	@Test
	public void testCellInitialB() {
		BoardCell cell = board.getCellAt(0, 0);
		assertEquals('B', cell.getInitial());
	}
	
	//20,0,A
	@Test
	public void testCellInitialA() {
		BoardCell cell = board.getCellAt(0, 20);
		assertEquals('A', cell.getInitial());
	}
}
