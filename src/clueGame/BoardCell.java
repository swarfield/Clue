package clueGame;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class BoardCell extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int row; //x coord
	private int col; //y coord
	private char initial;
	private DoorDirection doorDir;
	private boolean isLable;

	public static final int BOX_WIDTH = 45;
	public static final int BOX_HEIGHT = 29;

	/**
	 * Constructor
	 * @param x x coordinate
	 * @param y y coordinate
	 */
	public BoardCell(int x, int y){
		this.col = x;
		this.row = y;
		doorDir = DoorDirection.NONE;
		isLable = false;
	}

	/**
	 * Default Constructor
	 */
	public BoardCell(){
		col = 0;
		row = 0;
		doorDir = DoorDirection.NONE;
		isLable = false;
	}


	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "BoardCell [row=" + row + ", col=" + col + "]";
	}


	public Boolean isWalkWay() {
		if (initial == 'W') return true;
		return false;
	}

	public Boolean isRoom() {
		if (initial == 'W' || doorDir != DoorDirection.NONE) return false;
		return true;
	}

	public Boolean isDoorway() {
		if (doorDir == DoorDirection.NONE) return false;
		return true;
	}
	/**
	 * returns the x coordinate of the cell
	 * @return row	Row is the x coordinate of the cell
	 */
	public int getRow() {
		return row;
	}
	/**
	 * sets the x coordinate of the cell
	 * @param row
	 */
	public void setRow(int row) {
		this.row = row;
	}
	/**
	 * returns the y coordinate of the cell
	 * @return col
	 */
	public int getCol() {
		return col;
	}
	/**
	 * sets the y coordinate of the cell
	 * @param col
	 */
	public void setCol(int col) {
		this.col = col;
	}

	public char getInitial() {
		return initial;
	}

	public void setInitial(char initial) {
		this.initial = initial;
	}
	public DoorDirection getDoorDirection() {
		return doorDir;
	}

	public void setDoorDirection(DoorDirection dir) {
		doorDir = dir;
	}

	/**
	 * Called by board to draw each cell
	 * @param g
	 */
	public void draw(Graphics g) {
		//set color
		if(initial == 'C') {
			g.setColor(Color.BLUE);
		} else if (isRoom() || isDoorway()) {
			g.setColor(Color.GRAY);
		} else {
			g.setColor(Color.YELLOW);
		}

		//fill rectangle
		g.fillRect(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);

		if (initial == 'W') {
			g.setColor(Color.BLACK);
			g.drawRect(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
		}

		g.setColor(Color.blue);
		if (isDoorway()) {
			switch (doorDir) {
			case UP:
				g.fillRect(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT/5);
				break;
			case DOWN:
				g.fillRect(col*BOX_WIDTH, (row+1)*BOX_HEIGHT-BOX_HEIGHT/5, BOX_WIDTH, BOX_HEIGHT/5);
				break;
			case LEFT:
				g.fillRect(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH/5, BOX_HEIGHT);
				break;
			case RIGHT:
				g.fillRect((col+1)*BOX_WIDTH-BOX_WIDTH/5, row*BOX_HEIGHT, BOX_WIDTH/5, BOX_HEIGHT);
				break;
			default:
				break;
			}
		}
	}

	public void draw(Graphics g, Color targeted) {
		//set color
		g.setColor(targeted);
		//fill rectangle
		g.fillRect(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);

		g.setColor(Color.BLACK);
		g.drawRect(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);

	}


	public void drawLable(Graphics g) {
		if (isLable) {
			g.setColor(Color.BLACK);
			g.drawString(Board.getInstance().getLegend().get(initial), col*BOX_WIDTH, row*BOX_HEIGHT);
		}
	}

	public void setLable(boolean isLable) {
		this.isLable = isLable;
	}
}
