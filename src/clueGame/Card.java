package clueGame;

/**
 * 
 * @author Ross Starritt
 * @author Sam Warfield
 * This class is the card class. The object created hold the card type and the card name.
 * There is also an equality function.
 *
 */
public class Card {
	
	private CardType type;
	private String name;
	
	/**
	 * Assigns the card type to t and the name to n.
	 * @param t, CardType
	 * @param n, String
	 */
	public Card(CardType t, String n) {
		this.type = t;
		this.name = n;
	}
	
	/**
	 * creates empty card
	 * @param t, CardType
	 */
	public Card(CardType t) {
		this.type = t;
		name = new String();
	}
	
	/**
	 * Determines if this card and otherCard are separate cards with the same information stored.
	 * @param otherCard, Card
	 * @return
	 */
	public Boolean equals(Card otherCard) {
		return (this.type == otherCard.type && this.name.equals(otherCard.name));
	}
	
	public String getName() {
		return name;
	}
	
	public CardType getType() {
		return type;
	}
}
