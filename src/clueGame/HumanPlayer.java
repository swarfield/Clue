package clueGame;

import java.util.Set;

import gui.GuessGui;

/**
 * The Player class that interacts with the user
 * @author Samuel Warfield
 * @author Ross Starritt
 */
public class HumanPlayer extends Player{
	private Board board;
	private Solution suggestion;
	
	public HumanPlayer(String name, String color, int col, int row) {
		super(name, color, col, row);
		
	}
	public void resetSug() {
		suggestion = null;
	}
	
	public void setSuggestion(Solution suggestion) {
		this.suggestion = suggestion;
	}

	public BoardCell pickLocation(int dist) {
		board = Board.getInstance();
		board.calcTargets(row, col, dist);
		Set<BoardCell>  targs = board.getTargets();
		board.setHumanTargeted(targs);
		board.repaint();
		
		return null;
	}

	@Override
	public Solution getSuggestion() {
		return suggestion;
	}
}
