package clueGame;
/**
 * Enum type for the direction that doors face on the board
 * @author Samuel Warfield
 * @author Ross Starrit
 */
public enum DoorDirection {
	UP, DOWN, LEFT, RIGHT, NONE
}
