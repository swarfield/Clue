package clueGame;

import java.util.ArrayList;
import java.util.Random;
import java.awt.Color; // BE SURE TO USE THIS IMPORT
import java.awt.Graphics;
//not the one Eclipse suggests
import java.lang.reflect.Field;
/**
 * Abstract class which the human and computer players inherit from
 * @author Samuel Warfirld
 * @author Ross Starritt
 */
public abstract class Player {
	
	private String name;
	private Color color;
	protected int col;
	protected int row;
	private ArrayList<Card> hand;
	private ArrayList<Card> seenCards;
	private ArrayList<Card> seenRooms;
	private ArrayList<Card> seenPersons;
	private ArrayList<Card> seenWeapons;
	private static int BOX_WIDTH;
	private static int BOX_HEIGHT;
	
	/**
	 * The Constructor
	 * @param name  Name of Player
	 * @param color Color of Player
	 * @param col Initial column
	 * @param row Initial row
	 */
	public Player(String name, String color, int col, int row) {
		hand = new ArrayList<Card>();
		seenCards = new ArrayList<Card>();
		seenRooms = new ArrayList<Card>();
		seenPersons = new ArrayList<Card>();
		seenWeapons = new ArrayList<Card>();
		this.name = name;
		this.color = convertColor(color);
		this.col = col;
		this.row = row;
	}
	
	/**
	 * adds card to the player's hand
	 * @param card
	 */
	public void deal(Card card) {
		hand.add(card);
		seeCard(card);
	}
	
	public Card disproveSuggestion(Solution aSoln){
		ArrayList<Card> possibleReturns = new ArrayList<Card>();
		Random rand = new Random();
		for (Card aCard : aSoln.getAnswer()) {
			if (hand.contains(aCard)) {
				possibleReturns.add(aCard);
			}
		}
		
		if (possibleReturns.size() == 0) {
			return null;
		}
		return possibleReturns.get(rand.nextInt(possibleReturns.size()));
	}
	
	/**
	 * Causes the player to pick a new location
	 * @param dist
	 * @return Returns the BoardCell the player has moved to
	 */
	public abstract BoardCell pickLocation(int dist);
	
	/**
	 * Forces the player to generate a sugesstion
	 * @return a solution object
	 */
	public abstract Solution getSuggestion();
	/**
	 * Takes a string and returns a color object
	 * @param strColor
	 * @return returns the color associated with that string
	 */
	public Color convertColor(String strColor) {
		Color color;
		try {
				//We can use reflection to convert the string to a color
				Field field = Class.forName("java.awt.Color").getField(strColor.trim());
				color = (Color)field.get(null);
		} catch (Exception e) {
			color = null; // Not defined
		}
		return color;
	}
	
	/**
	 * adds a card to the appropriate seen card list
	 * @param c
	 */
	public void seeCard(Card card) {
		seenCards.add(card);
		if (card.getType() == CardType.ROOM) {
			seenRooms.add(card);
		} else if (card.getType() == CardType.PERSON) {
			seenPersons.add(card);
		} else if (card.getType() == CardType.WEAPON) {
			seenWeapons.add(card);
		}
	}

	public String getName() {
		return name;
	}

	public Color getColor() {
		return color;
	}

	public int getCol() {
		return col;
	}

	public int getRow() {
		return row;
	}

	public ArrayList<Card> getHand() {
		return hand;
	}

	public ArrayList<Card> getSeenCards() {
		return seenCards;
	}

	public void setSeenCards(ArrayList<Card> seenCards) {
		this.seenCards = seenCards;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public void setHand(ArrayList<Card> hand) {
		this.hand = hand;
	}

	public ArrayList<Card> getSeenRooms() {
		return seenRooms;
	}

	public ArrayList<Card> getSeenPersons() {
		return seenPersons;
	}

	public ArrayList<Card> getSeenWeapons() {
		return seenWeapons;
	}
	
	public void draw(Graphics g) {
		//System.out.println(name + ' ' + Integer.toString(col) + ' ' + Integer.toString(row));
		Board.getInstance().getCellAt(0, 0);
		BOX_HEIGHT = BoardCell.BOX_HEIGHT;
		Board.getInstance().getCellAt(0, 0);
		BOX_WIDTH = BoardCell.BOX_WIDTH;
		g.setColor(color);
		g.fillOval(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
		g.setColor(Color.BLACK);
		g.drawOval(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
	}
	
	public void draw(Graphics g, Color targeted) {
		//System.out.println(name + ' ' + Integer.toString(col) + ' ' + Integer.toString(row));
		Board.getInstance().getCellAt(0, 0);
		BOX_HEIGHT = BoardCell.BOX_HEIGHT;
		Board.getInstance().getCellAt(0, 0);
		BOX_WIDTH = BoardCell.BOX_WIDTH;
		g.setColor(targeted);
		g.fillOval(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
		g.setColor(Color.BLACK);
		g.drawOval(col*BOX_WIDTH, row*BOX_HEIGHT, BOX_WIDTH, BOX_HEIGHT);
	}
	
	public BoardCell getBoardCell() {
		return Board.getInstance().getCellAt(row, col);
	}
	
	public void setBoardCell(BoardCell newPlace) {
		row = newPlace.getRow();
		col = newPlace.getCol();
	}
}
