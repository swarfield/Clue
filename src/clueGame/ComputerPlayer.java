package clueGame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import gui.SplashScreen;

/**
 * This is the player class for all CPU Characters
 * @author Samuel Warfield
 * @author Ross Starritt
 */
public class ComputerPlayer extends Player{
	char roomLastVisited; // Keeps track of the last room visited by CPU Character
	ArrayList<String> roomsLastVisited;
	Solution suggestion;
	
	//TODO: Computer Turn
	/*
	I have a makeMove method for the computer player that does the following:

    picks a location from the calculated list. This method was tested in a previous lab.
    moves to that location.
    this method also handles suggestions and accusations, which we'll add in the next lab.
	 */
	
	/**
	 * Basically calls the super class constructor.
	 * @param name Name of the player
	 * @param color Color of the player
	 * @param col First coordinate for player location
	 * @param row Second coordinate for player location
	 */
	public ComputerPlayer(String name, String color, int col, int row) {
		super(name, color, col, row);

		suggestion = new Solution();
		roomsLastVisited = new ArrayList<String>();
		roomLastVisited = '0';
	}
	
	public void clearVisitedList() {
		roomsLastVisited.clear();
	}
	
	/**
	 * Picks the location that the CPU Character moves to next turn
	 * @param targets takes the return Board.calcTargets
	 * @return Returns the Cell that the player will move to
	 */
	public BoardCell pickLocation(int dist) {
		
		Random random = new Random();
		Board board = Board.getInstance();
		
		board.calcTargets(row, col, dist);
		Set<BoardCell> targets = board.getTargets();
		
		if (board.getCellAt(this.row, this.col).getInitial() != 'W') {
			roomLastVisited = board.getCellAt(this.row, this.col).getInitial();
		}

		// Check if there are rooms
		ArrayList<BoardCell> roomTargs = new ArrayList<BoardCell>();
		for(BoardCell aTarget: targets) {
			if (aTarget.isDoorway()) {
				roomTargs.add(aTarget);
			}
		}
		
		//Remove the the last room visited if present
		ArrayList<BoardCell> sameStuff= new ArrayList<BoardCell>();
		for(BoardCell hi :roomTargs) {
			if (roomsLastVisited.contains(board.getLegend().get(hi.getInitial()))) {
				sameStuff.add(hi);
			}
		}
		
		roomTargs.removeAll(sameStuff);
		
		//if there is more than 0 rooms present choose from the list
		if (roomTargs.size() > 0) {
			BoardCell aRoom = roomTargs.get(random.nextInt() % roomTargs.size());
			addToRoomsList(board.getLegend().get(aRoom.getInitial()));
			roomLastVisited = aRoom.getInitial();
			row = aRoom.getRow();
			col = aRoom.getCol();
			
			return aRoom;
		}

		// If no rooms are available
		int i = random.nextInt(targets.size());
		
		Iterator<BoardCell> iter = targets.iterator();
		for(int j = 0; j<i; j++) {
			iter.next();
		}
		BoardCell aTarget = iter.next();
		row = aTarget.getRow();
		col = aTarget.getCol();
		
		return aTarget;
	}

	private void addToRoomsList(String newToAdd) {
		if (roomsLastVisited.size() == 4) {
			roomsLastVisited.remove(3);
			roomsLastVisited.add(0, newToAdd);
		} else {
			roomsLastVisited.add(newToAdd);
		}
	}
	
	/**
	 * Creates the accusation for the computer player
	 */
	public void makeAccusation() { //TODO: Finish This
		if (getSeenCards().size() == Board.getInstance().getDeck().size() - 3) {
			//do stuff
			ArrayList<Card> deck = Board.getInstance().getDeck();
			deck.removeAll(getSeenCards());
			Solution soln = new Solution();
			for (Card c: deck) {
				if (c.getType() == CardType.PERSON) {
					soln.setPerson(c);
				} else if (c.getType() == CardType.WEAPON) {
					soln.setWeapon(c);
				} else if (c.getType() == CardType.ROOM) {
					soln.setRoom(c);
				}
			}
			if(Board.getInstance().checkAccusation(soln)) {
				new SplashScreen(getName() + "Wins!");
				System.exit(0);
			} else {
				new SplashScreen(getName() + " Loses!");
				Board.getInstance().getPlayers().remove(this);
				for (Card c: getHand()) {
					for (Player pl: Board.getInstance().getPlayers()) {
						pl.seeCard(c);
					}
				}
			}
		}
	}

	/**
	 * Creates a suggestion for the computer player
	 */
	public void createSuggestion() {
		suggestion.clear();
		Random random = new Random();
		ArrayList<Card> deck = new ArrayList<Card>();
		deck.addAll(Board.getInstance().getDeck());
		deck.removeAll(getSeenCards());
		ArrayList <Card> weapon = new ArrayList<Card>();
		ArrayList <Card> person = new ArrayList <Card>();
		for(Card card: deck) {
			if (card.getType() == CardType.WEAPON) {
				weapon.add(card);
			} else if (card.getType() == CardType.PERSON) {
				person.add(card);
			}
		}
		
		suggestion.setPerson(person.get(random.nextInt(person.size())));
		suggestion.setWeapon(weapon.get(random.nextInt(weapon.size())));
		Card room = new Card(CardType.ROOM,Board.getInstance().getLegend().get(roomLastVisited));
		suggestion.setRoom(room);
	}
	
	public Solution getSuggestion() {
		createSuggestion();
		return suggestion;
	}
	
	public void setRoomLastVisited(char roomLastVisited) {
		this.roomLastVisited = roomLastVisited;

	}
}
