package clueGame;

import java.util.ArrayList;

/**
 * 
 * @author Ross Starritt, Sam Warfield
 * 
 * This class contains three Cards, weapon, person, and room. 
 * The cards represent the three parts of a complete Clue solution.
 * The data is passed to other classes in the form of an ArrayList of Cards
 *
 */
public class Solution {
	private Card weapon;
	private Card person;
	private Card room;
	
	/**
	 * Allocates space in memory for the three cards required for a complete solution.
	 */
	public Solution() {
		this.weapon = new Card(CardType.WEAPON);
		this.person = new Card(CardType.PERSON);
		this.room = new Card(CardType.ROOM);
	}
	
	/**
	 * Creates a solution with the 3 cards passed in.
	 * @param w, Card, Should be a weapon
	 * @param p, Card, Should be a person
	 * @param r, Card, Should be a room
	 */
	public Solution(Card w, Card p, Card r) {
		this.setWeapon(w);
		this.setPerson(p);
		this.setRoom(r);
	}
	
	/**
	 * adds a weapon to the solution
	 * @param c, Card, should be a weapon
	 */
	public void setWeapon(Card c) {
		if (c.getType() != CardType.WEAPON) {
			System.out.println("expected type, weapon, but was " + c.getType().name());
		} else {
			this.weapon = c;
		}
	}
	
	/**
	 * adds a person to the solution
	 * @param c, Card, should be a person
	 */
	public void setPerson(Card c) {
		if (c.getType() != CardType.PERSON) {
			System.out.println("expected type, person, but was " + c.getType().name());
		} else {
			this.person = c;
		}
	}
	
	/**
	 * adds a room to the solution
	 * @param c, Card, should be a room
	 */
	public void setRoom(Card c) {
		if (c.getType() != CardType.ROOM) {
			System.out.println("expected type, room, but was " + c.getType().name());
		} else {
			this.room = c;
		}
	}
	
	/**
	 * 
	 * @return returns an ArrayList<Card> containing a person, a room, and a weapon.
	 */
	public ArrayList<Card> getAnswer() {
		ArrayList<Card> soln = new ArrayList<Card>();
		soln.add(person);
		soln.add(room);
		soln.add(weapon);
		return soln;
	}

	public Card getWeapon() {
		return weapon;
	}

	public Card getPerson() {
		return person;
	}

	public Card getRoom() {
		return room;
	}
	
	public Solution getSolution() {
		return this;
	}
	public boolean equals(Solution soln2) {
		return this.person.equals(soln2.person) && this.room.equals(soln2.room) && this.weapon.equals(soln2.weapon);
	}

	public void clear() {
		person = null;
		room = null;
		weapon = null;		
	}
}
