package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clueGame.*;

public class GameActionTests {

	// NOTE: I made Board static because I only want to set it up one 
	// time (using @BeforeClass), no need to do setup before each test.
	private static Board board;
	private static ArrayList<Card> theDeck;
	private static ArrayList<Player> allPlayers;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		// Board is singleton, get the only instance
		board = Board.getInstance();
		// set the file names to use my config files
		board.setConfigFiles("Clues.csv", "roomKey.txt", "people.csv", "weapons.csv");		
		// Initialize will load BOTH config files 
		board.initialize();
	}

	@Before
	public void setUpTestVars() { // This is so that we are memory safe between all 
		//tests
		theDeck = new ArrayList<Card>();
		allPlayers = new ArrayList<Player>();

		//This makes it so that we can change theDeck and etc. Without changing the initial
		//board state of other tests
		theDeck.addAll(board.getDeck());
		allPlayers.addAll(board.getPlayers());
		board.getCpuPlayers();
	}

	@Test
	public void cpuTargetingtest() { // Tests the movement of the Computer Players.
		// All Target test have been added to Clues.ods

		// TEST1: If there is no room in the target list then choose randomly
		ComputerPlayer Meeseeks = new ComputerPlayer("Mr. Meeseeks", "Blue", 11,17);
		HashSet<BoardCell> possibleLocations = new HashSet<BoardCell>();

		int loop = 0;
		board.calcTargets(Meeseeks.getRow(), Meeseeks.getCol(), 3);

		while(loop < 1000) {

			BoardCell location = Meeseeks.pickLocation(3);
			possibleLocations.add(location);

			if(possibleLocations.size() == board.getTargets().size()) {
				break;
			}

			loop++;
		}

		// Unless we're really unlucky after 1000 trials there should be as many picked
		// locations as possible targets if the locations are truly random.
		assertEquals(possibleLocations.size(), board.getTargets().size());




		// TEST2: If the list includes a room, it should be chosen 

		Meeseeks.setCol(11);
		Meeseeks.setRow(10);

		board.calcTargets(Meeseeks.getRow(), Meeseeks.getCol(), 3);

		for (int i = 0; i < 1000; i++) { // Makes sure that the answer is reliable
			Meeseeks.setRoomLastVisited('W');
			Meeseeks.clearVisitedList();
			Meeseeks.setCol(11);
			Meeseeks.setRow(10);
			
			Meeseeks.pickLocation(3);

			assertEquals(8,Meeseeks.getRow()); // location of the the door to the room
			assertEquals(12,Meeseeks.getCol());
		}

		// TEST3 If player was just in that room. In that case,
		// the room has the same chance of being chosen as any other target.

		Meeseeks.setCol(12); // a door with another door immediately adjacent to it to 
		Meeseeks.setRow(8);  // the same room.

		board.calcTargets(Meeseeks.getRow(), Meeseeks.getCol(), 4);
		loop = 0;
		possibleLocations.clear();

		while(loop < 1000) {
			BoardCell location = Meeseeks.pickLocation(4);
			possibleLocations.add(location);

			if(possibleLocations.size() == board.getTargets().size()) {
				break;
			}

			loop++;
		}
		//System.out.println(loop);
		// Unless we're really unlucky after 1000 trials there should be as many picked
		// locations as possible targets if the locations are truly random.
		//System.out.println(possibleLocations.size());
		//System.out.println(board.getTargets().size());
		assertEquals(possibleLocations.size(), board.getTargets().size());

		// TEST4: Test the case where players can bounce between rooms

	}

	@Test
	public void checkAccusationTest() {
		//create solution
		board.getSolution().setPerson(new Card(CardType.PERSON, "Mr. Brown"));
		board.getSolution().setRoom(new Card(CardType.ROOM, "Dormroom"));
		board.getSolution().setWeapon(new Card(CardType.WEAPON, "Exam"));		
		//test correct accusation
		assertTrue(board.checkAccusation(new Solution(new Card(CardType.WEAPON, "Exam"), new Card(CardType.PERSON, "Mr. Brown"), new Card(CardType.ROOM, "Dormroom"))));
		assertFalse(board.checkAccusation(new Solution(new Card(CardType.WEAPON, "Rusty Hard Disk Drive"), new Card(CardType.PERSON, "Mr. Brown"), new Card(CardType.ROOM, "Dormroom"))));
		assertFalse(board.checkAccusation(new Solution(new Card(CardType.WEAPON, "Exam"), new Card(CardType.PERSON, "Col. Cyan"), new Card(CardType.ROOM, "Dormroom"))));
		assertFalse(board.checkAccusation(new Solution(new Card(CardType.WEAPON, "Exam"), new Card(CardType.PERSON, "Mr. Brown"), new Card(CardType.ROOM, "Study"))));
	}

	@Test
	public void cpuSuggestionTest() {
		// generate a cpu suggestion
		ComputerPlayer cpu = new ComputerPlayer("Col. Cyan", "cyan", 20, 21);

		Solution soln = new Solution(new Card(CardType.WEAPON, "Exam"), new Card(CardType.PERSON, "Mr. Brown"), new Card(CardType.ROOM, "Dormroom"));

		for (Card c: theDeck) {
			boolean inSoln = false;
			for (Card solnCard: soln.getAnswer()) {
				if (solnCard.equals(c)) {
					inSoln = true;
				}
			}
			if (inSoln) {
				continue;
			}
			cpu.seeCard(c);
		}
		board.calcTargets(20, 21, 1);
		cpu.pickLocation(1);
		cpu.createSuggestion();
		assertTrue(soln.equals(cpu.getSuggestion()));
		// room should be current room
		cpu.getSeenCards().clear();
		for (Card c: theDeck) {
			boolean inSoln = false;
			for (Card solnCard: soln.getAnswer()) {
				if (solnCard.equals(c)) {
					inSoln = true;
				}
			}
			if (inSoln) {
				continue;
			} else if (c.equals(new Card(CardType.PERSON, "Ms. Blue")) || c.equals(new Card(CardType.WEAPON, "Gun"))) {
				continue;
			}
			cpu.seeCard(c);
		}
		HashSet<Card> peopleBoi = new HashSet<Card>();
		HashSet<Card> weaponBoi = new HashSet<Card>();
		for (int i = 0; i < 1000; i++) {
			cpu.createSuggestion();
			peopleBoi.add(cpu.getSuggestion().getPerson());
			weaponBoi.add(cpu.getSuggestion().getWeapon());
		}
		assertEquals(2, peopleBoi.size());
		assertEquals(2, weaponBoi.size());
		// randomly choose from not seen weapons and people cards

	}

	@Test
	public void boardHandleSuggestion() {
		//TEST1: Suggestion no one can disprove returns null
		Solution finalSoln = board.getSolution();

		for (int i = 0; i < board.getPlayers().size(); i++) {
			//System.out.println(board.handleSuggestion(board.getPlayers().get(i), finalSoln));
			assertTrue(board.handleSuggestion(board.getPlayers().get(i), finalSoln) == null); //this should be null since no one has these cards
		}



		//TEST2: Suggestion only accusing player can disprove returns null
		//make new player
		ComputerPlayer tempPlayer = new ComputerPlayer("Meeseeks2", "Blue", 0, 0);

		//make their hand
		ArrayList<Card> hand = new ArrayList<Card>();
		hand.add(new Card(CardType.WEAPON,"yourface")); // solutions
		hand.add(new Card(CardType.PERSON,"Mr. Brown")); 
		hand.add(new Card(CardType.ROOM,"yourface")); 

		//Rig the suggestion
		Solution cheaty = new Solution(hand.get(0),hand.get(1),hand.get(2));

		//now test
		tempPlayer.setHand(hand);
		assertEquals(null, board.handleSuggestion(tempPlayer, cheaty));



		//TEST3: Suggestion only human can disprove returns answer (i.e., card that disproves suggestion)
		//get Human player
		ArrayList<Player> humanFinder = new ArrayList<Player>();
		//System.out.println(board.getPlayers());
		humanFinder.addAll(board.getPlayers());
		humanFinder.removeAll(board.getCpuPlayers());// Leaves Us with only the human player

		Player human = humanFinder.get(0);

		human.setHand(hand);

		//DEP
		//assertNotEquals(null, board.handleSuggestion(board.getCpuPlayers().get(0), cheaty));



		//TEST4:Suggestion only human can disprove, but human is accuser, returns null

		assertEquals(null, board.handleSuggestion(human, cheaty)); // all variable setup is in TEST3.

		//TEST5:Suggestion that two players can disprove, correct player (based on starting with next player in list) returns answer

		// Get 2 CPUS, look for one card of each type

		Player cpu1 = board.getCpuPlayers().get(0);
		//make their hand
		hand = new ArrayList<Card>();
		hand.add(new Card(CardType.WEAPON,"yourface"));
		hand.add(new Card(CardType.PERSON,"yourface")); 
		hand.add(new Card(CardType.ROOM,"yourface"));
		cpu1.setHand(hand);

		Player cpu2 = board.getCpuPlayers().get(1);
		//make their hand
		hand = new ArrayList<Card>();
		hand.add(new Card(CardType.WEAPON,"yourface"));
		hand.add(new Card(CardType.PERSON,"yourface")); 
		hand.add(new Card(CardType.ROOM,"yourface"));
		cpu2.setHand(hand);

		// make a suggestion

		Solution multiPersonSoln = new Solution(cpu1.getHand().get(0),cpu2.getHand().get(1),cpu1.getHand().get(2));

		// Test Suggestion return
		ArrayList<Card> disproves = board.handleSuggestion(human, multiPersonSoln);
		//assertEquals(2, disproves.size());
		//assertTrue(disproves.contains(cpu2.getHand().get(1)));
		//assertTrue(disproves.contains(cpu1.getHand().get(0)) || disproves.contains(cpu1.getHand().get(2)));



		//TEST6: Suggestion that human and another player can disprove, other player is next in list, ensure other player returns answer
		multiPersonSoln = new Solution(human.getHand().get(0), cpu1.getHand().get(1), cpu1.getHand().get(2));
		
		for (int i =0; i < 1000; i++) {
		//assertTrue(board.handleSuggestion(cpu2, multiPersonSoln).contains(cpu1.getHand().get(1)) 
				//|| board.handleSuggestion(cpu2, multiPersonSoln).contains(cpu1.getHand().get(2))
				//|| board.handleSuggestion(cpu2, multiPersonSoln).contains(human.getHand().get(0)));
		}
	}
	
	@Test //tests the disproveSuggestion of the Player Class
	public void disproveSuggestionTest() {
		Solution temp; //Temp Var
		HashSet<Card> tempCard = new HashSet<Card>();
	    
		
		
		//If player has only one matching card it should be returned
		
		//HumanPlayer
		ArrayList<Card> hand = new ArrayList<Card>();
		hand.add(new Card(CardType.WEAPON,"yourface"));
		hand.add(new Card(CardType.PERSON,"yourface")); 
		hand.add(new Card(CardType.ROOM,"yourface"));
		allPlayers.get(0).setHand(hand); 

		temp = new Solution(allPlayers.get(0).getHand().get(0),
				            new Card(CardType.PERSON,"yourface"),new Card(CardType.ROOM,"yourface"));
		
		assertNotEquals(null,allPlayers.get(0).disproveSuggestion(temp));
		
		
		//ComputerPlayer		ArrayList<Card> hand = new ArrayList<Card>();
		hand.add(new Card(CardType.WEAPON,"yourface"));
		hand.add(new Card(CardType.PERSON,"yourface")); 
		hand.add(new Card(CardType.ROOM,"yourface"));
		allPlayers.get(1).setHand(hand); 

		temp = new Solution(allPlayers.get(1).getHand().get(0),
				            new Card(CardType.PERSON,"yourface"),new Card(CardType.ROOM,"yourface"));
		
		assertNotEquals(null,allPlayers.get(1).disproveSuggestion(temp));
		
		
		
	    //If players has >1 matching card, returned card should be chosen randomly
		//HumanPlayer
		hand.add(new Card(CardType.WEAPON,"yourface"));
		hand.add(new Card(CardType.PERSON,"yourface")); 
		hand.add(new Card(CardType.ROOM,"yourface"));
		allPlayers.get(0).setHand(hand); 

		temp = new Solution(allPlayers.get(0).getHand().get(0),
							allPlayers.get(0).getHand().get(1),new Card(CardType.ROOM,"yourface"));
		
		assertNotEquals(null,allPlayers.get(0).disproveSuggestion(temp));
		
		for (int i = 0; i < 1000; i++) {
			tempCard.add(allPlayers.get(0).disproveSuggestion(temp));
			if (tempCard.size() == 2) {
				break;
			}
		}
		assertEquals(2,tempCard.size());
		tempCard.clear();
		
		//ComputerPlayer
		hand.add(new Card(CardType.WEAPON,"yourface"));
		hand.add(new Card(CardType.PERSON,"yourface")); 
		hand.add(new Card(CardType.ROOM,"yourface"));
		allPlayers.get(1).setHand(hand); 

		temp = new Solution(allPlayers.get(1).getHand().get(0),
							allPlayers.get(1).getHand().get(1),new Card(CardType.ROOM,"yourface"));
		
		assertNotEquals(null,allPlayers.get(1).disproveSuggestion(temp));
		
		for (int i = 0; i < 1000; i++) {
			tempCard.add(allPlayers.get(1).disproveSuggestion(temp));
			if (tempCard.size() == 2) {
				break;
			}
		}
		assertEquals(2,tempCard.size());
		tempCard.clear();
		
		
		
		//If player has no matching cards, null is returned
		assertEquals(null, board.getPlayers().get(1).disproveSuggestion(board.getSolution())); // no one can disprove the actual solution
	}
}
