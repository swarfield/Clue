package tests;

import clueGame.BoardCell;

import java.util.HashSet;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import clueGame.Board;

public class targetingTest {
	private static Board board;
	//Setup

	@BeforeClass
	public static void setUp() {
		// Board is singleton, get the only instance
		board = Board.getInstance();
		// set the file names to use my config files
		board.setConfigFiles("Clues.csv", "roomKey.txt");		
		// Initialize will load BOTH config files 
		board.initialize();
	}
	//Testing New Adjacency Calcs
	
	//only walkways adjacent
	@Test
	public void adjWalkways() {
		HashSet<BoardCell> adjList = new HashSet<BoardCell>();
		
		adjList = board.getAdjList(board.getCellAt(23, 6)); //   row 23, col 6
		assertTrue(adjList.contains(board.getCellAt(22, 6))); // row 22, col 6
		assertTrue(adjList.contains(board.getCellAt(24, 6))); // row 24, col 6
		assertTrue(adjList.contains(board.getCellAt(23, 5))); // row 23, col 5
		assertTrue(adjList.contains(board.getCellAt(23, 7))); // row 23, col 7
		assertEquals(4, adjList.size());
		
		adjList = board.getAdjList(board.getCellAt(23, 18)); //   row 23, col 18
		assertTrue(adjList.contains(board.getCellAt(22, 18))); // row 22, col 18
		assertTrue(adjList.contains(board.getCellAt(24, 18))); // row 24, col 18
		assertTrue(adjList.contains(board.getCellAt(23, 17))); // row 23, col 17
		assertTrue(adjList.contains(board.getCellAt(23, 19))); // row 23, col 19
		assertEquals(4, adjList.size());
		
		adjList = board.getAdjList(board.getCellAt(8, 6)); //   row 8, col 6
		assertTrue(adjList.contains(board.getCellAt(7, 6))); // row 7, col 6
		assertTrue(adjList.contains(board.getCellAt(9, 6))); // row 9, col 6
		assertTrue(adjList.contains(board.getCellAt(8, 5))); // row 8, col 5
		assertTrue(adjList.contains(board.getCellAt(8, 7))); // row 8, col 7
		assertEquals(4, adjList.size());
		
		adjList = board.getAdjList(board.getCellAt(8, 18)); //   row 8, col 18
		assertTrue(adjList.contains(board.getCellAt(7, 18))); // row 7, col 18
		assertTrue(adjList.contains(board.getCellAt(9, 18))); // row 9, col 18
		assertTrue(adjList.contains(board.getCellAt(8, 17))); // row 8, col 17
		assertTrue(adjList.contains(board.getCellAt(8, 19))); // row 8, col 19
		assertEquals(4, adjList.size());
	}
	
	//locs within rooms
	@Test
	public void adjInRoom() {
		HashSet<BoardCell> adjList = new HashSet<BoardCell>();
		
		adjList = board.getAdjList(board.getCellAt(26, 2));
		assertTrue(adjList.isEmpty());
		
		adjList = board.getAdjList(board.getCellAt(23, 23));
		assertTrue(adjList.isEmpty());
		
		adjList = board.getAdjList(board.getCellAt(22, 21));
		assertTrue(adjList.isEmpty());
		
		adjList = board.getAdjList(board.getCellAt(0, 0));
		assertTrue(adjList.isEmpty());
	}
	
	//board edge cells
	@Test
	public void adjBoardEdge() {
		HashSet<BoardCell> adjList = new HashSet<BoardCell>();
		
		adjList = board.getAdjList(board.getCellAt(29, 0));
		assertTrue(adjList.isEmpty());
		
		adjList = board.getAdjList(board.getCellAt(29, 6));
		assertTrue(adjList.contains(board.getCellAt(29, 7)));
		assertTrue(adjList.contains(board.getCellAt(28, 6)));
		assertEquals(2, adjList.size());
		
		adjList = board.getAdjList(board.getCellAt(17, 25));//above, below, left
		assertTrue(adjList.contains(board.getCellAt(16, 25)));
		assertTrue(adjList.contains(board.getCellAt(18, 25)));
		assertTrue(adjList.contains(board.getCellAt(17, 24)));
		assertEquals(3, adjList.size());
		
		adjList = board.getAdjList(board.getCellAt(0, 6)); //0 6, right down
		assertTrue(adjList.contains(board.getCellAt(0, 7)));
		assertTrue(adjList.contains(board.getCellAt(1, 6)));
		assertEquals(2, adjList.size());
	}
	
	//next to a wall
	@Test
	public void adjWall() {
		HashSet<BoardCell> adjList = new HashSet<BoardCell>();
		
		adjList = board.getAdjList(board.getCellAt(27, 6)); //27, 6; up, down, right
		assertTrue(adjList.contains(board.getCellAt(27, 7)));
		assertTrue(adjList.contains(board.getCellAt(26, 6)));
		assertTrue(adjList.contains(board.getCellAt(28, 6)));
		assertEquals(3, adjList.size());
		
		adjList = board.getAdjList(board.getCellAt(27, 12)); //27, 12, up, down
		assertTrue(adjList.contains(board.getCellAt(28, 12)));
		assertTrue(adjList.contains(board.getCellAt(26, 12)));
		//assertTrue(adjList.contains(board.getCellAt(27, 13))); // Deprecated, 
		//board layout changed
		//assertEquals(3, adjList.size());		
	}
	
	// adj door
	@Test
	public void adjDoor() {
		HashSet<BoardCell> adjList = new HashSet<BoardCell>();
		
		adjList = board.getAdjList(board.getCellAt(19, 5));
		assertTrue(adjList.contains(board.getCellAt(20, 5)));
		assertFalse(adjList.contains(board.getCellAt(19, 6)));
		assertFalse(adjList.contains(board.getCellAt(18, 5)));
		assertFalse(adjList.contains(board.getCellAt(19, 4)));
		assertEquals(1, adjList.size());
	}
	
	@Test //Locations that are doorways (should have only one adjacent cell)

	public void testingSingletonDoors() {
		assertTrue(board.getAdjList(27, 17).size() == 1);
		assertTrue(board.getAdjList(27, 17).contains(board.getCellAt(27, 18)));
		
		assertTrue(board.getAdjList(0, 8).size() == 1);
		assertTrue(board.getAdjList(0, 8).contains(board.getCellAt(0, 7)));
		
		assertTrue(board.getAdjList(0, 17).size() == 1);
		assertTrue(board.getAdjList(0, 17).contains(board.getCellAt(0, 18)));

	}
	
	//TESTING TARGETING SOFTWARE
	
	@Test
	public void targetingTestWalkways() {
		//Testing cell at (20, 7)
		board.calcTargets(20, 7, 1); //distance of 1
		HashSet<BoardCell> targets = board.getTargets();
		assertTrue(targets.size() == 4);
		
		assertTrue(targets.contains(board.getCellAt(20, 6)));
		assertTrue(targets.contains(board.getCellAt(20, 8)));
		assertTrue(targets.contains(board.getCellAt(19, 7)));
		assertTrue(targets.contains(board.getCellAt(21, 7)));

		board.calcTargets(20, 7, 2); //distance of 2
		targets = board.getTargets();
		assertEquals(6,targets.size());

		assertTrue(targets.contains(board.getCellAt(18, 7)));
		assertTrue(targets.contains(board.getCellAt(22, 7)));
		assertTrue(targets.contains(board.getCellAt(19, 6)));
		assertTrue(targets.contains(board.getCellAt(21, 6)));
		assertTrue(targets.contains(board.getCellAt(20, 5)));
		assertTrue(targets.contains(board.getCellAt(19, 8)));


		//Testing cell at (20, 7)
		board.calcTargets(16, 11, 1); //distance of 1
		targets = board.getTargets();
		assertTrue(targets.size() == 1);

		assertTrue(targets.contains(board.getCellAt(17, 11)));

		board.calcTargets(16, 11, 2); //distance of 2
		targets = board.getTargets();
		assertTrue(targets.size() == 2);

		assertTrue(targets.contains(board.getCellAt(18, 11)));
		assertTrue(targets.contains(board.getCellAt(17, 12)));


		//Testing cell at (25, 19)
		board.calcTargets(25, 19, 1); //distance of 1
		targets = board.getTargets();
		assertTrue(targets.size() == 4);

		assertTrue(targets.contains(board.getCellAt(24, 19)));
		assertTrue(targets.contains(board.getCellAt(26, 19)));
		assertTrue(targets.contains(board.getCellAt(25, 18)));
		assertTrue(targets.contains(board.getCellAt(25, 20)));

		board.calcTargets(25, 19, 2); //distance of 2
		targets = board.getTargets();
		assertTrue(targets.size() == 6);

		assertTrue(targets.contains(board.getCellAt(23, 19)));
		assertTrue(targets.contains(board.getCellAt(27, 19)));
		assertTrue(targets.contains(board.getCellAt(24, 18)));
		assertTrue(targets.contains(board.getCellAt(26, 18)));
		assertTrue(targets.contains(board.getCellAt(24, 20)));
		assertTrue(targets.contains(board.getCellAt(25, 21)));
	}

	@Test
	public void targetingRoomEntries() {
		//Testing cell at (25, 6)
		board.calcTargets(25, 6, 1);
		HashSet<BoardCell> targets = board.getTargets();
		
		assertTrue(!targets.contains(board.getCellAt(25, 5)));
		
		//Testing cell at (28, 12)
		board.calcTargets(28, 12, 2); //distance of 2
		//System.out.println(board.getAdjList(29,12));
		targets = board.getTargets();
		
		assertTrue(targets.contains(board.getCellAt(29, 11)));
		//assertTrue(targets.contains(board.getCellAt(27, 13))); //DEP
		assertTrue(!targets.contains(board.getCellAt(29, 13)));
		
		//Testing cell at (28, 12)
		board.calcTargets(28, 12, 4); //distance of 1
		targets = board.getTargets();
		
		//Layout CHange Depreciated this
		//assertTrue(targets.contains(board.getCellAt(27, 13)));
	}
	
	@Test
	public void targetingRoomExits() {
		//Testing cell at (27, 17)
		board.calcTargets(27, 17, 2);
		HashSet<BoardCell> targets = board.getTargets();
		
		
		assertTrue(targets.contains(board.getCellAt(26, 18)));
		assertTrue(targets.contains(board.getCellAt(28, 18)));
		assertTrue(targets.contains(board.getCellAt(27, 19)));
		assertEquals(3, targets.size());
	}
}
